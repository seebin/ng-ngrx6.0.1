# 版本

Angular CLI：6.0.1
@ngrx/store：6.0.1

## 开发服务器

运行 `ng serve` 启动服务. 访问 `http://localhost:4200/`.

store模块文档：
https://github.com/ngrx/platform/blob/master/docs/store/README.md


不同分支对应不同的功能：  
最最基础的例子 store  
 [https://gitee.com/seebin/ng-ngrx6.0.1/tree/master/](https://gitee.com/seebin/ng-ngrx6.0.1/tree/master/)

一个浏览器扩展工具，可进行时间旅行，查看状态树等操作  
[https://gitee.com/seebin/ng-ngrx6.0.1/tree/devtools/](https://gitee.com/seebin/ng-ngrx6.0.1/tree/devtools/)

store-core  
[https://gitee.com/seebin/ng-ngrx6.0.1/tree/store-core/](https://gitee.com/seebin/ng-ngrx6.0.1/tree/store-core/)

